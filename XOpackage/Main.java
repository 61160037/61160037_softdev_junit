package XOpackage;

import java.util.Scanner;

public class Main{

    public static void main(String[] args) {
        char[][] XO = new char[3][3];
        Table table = new Table(XO);
        Input input = new Input();
        Checkwin check = new Checkwin();
        Print print = new Print();
        int sum = 0;
        int End = check.checkOxwin();
        
        System.out.println("Welcome to OX Game");
        table.setTable(XO);
        table.getTable(XO);
        while (true) {
            input.inputOx(XO,sum);
            table.getTable(XO);
            if(check.checkwin(XO,sum)){
                break;
            }
            check.checkDraw();
            sum++;
        }
        print.printXwin(End);
        print.printOwin(End);
        print.printDraw(End);
        

    }

}
